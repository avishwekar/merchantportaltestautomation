package com.code.java.api;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import static org.testng.Assert.fail;

import java.net.MalformedURLException;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;


import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import java.net.URL;

public class StartWebDriver {
	  public static WebDriver driver;
	  private String baseUrl;
	  private boolean acceptNextAlert = true;
	  private StringBuffer verificationErrors = new StringBuffer();
	  
	  
	  @Parameters({"url","user","password","priority"})
	  @BeforeClass(alwaysRun = true)
	  public void setUp(String url, String user, String password, boolean priority) throws Exception {
//	    driver = new FirefoxDriver();
			System.setProperty("webdriver.chrome.driver", "chromedriver");
			driver = new ChromeDriver();
			driver.get(url);
	    driver.manage().window().maximize();
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		TextBoxHelper.typeInTextBox("signinEmail", user);

	    TextBoxHelper.typeInTextBox("signinPassword", password);
	    driver.findElement(By.xpath("//button[@type='submit']")).click();Thread.sleep(9000);
	    if(priority) {
	        driver.findElement(By.xpath("//a[position()=1]/h4")).click();Thread.sleep(8000);
	    	driver.findElement(By.xpath("//button[text()='Skip']")).click();Thread.sleep(8000);
	    }
	    
		}

	
	public static void waitTillPageExists() throws Exception {
	   Thread.sleep(6000);
	}
	
	public static WebElement getElement(String locator) {
		boolean flag = false;
		if(locator.contains("/"))
			flag = true;
		if(driver.findElements(By.id(locator)).size() ==  1) {
			return driver.findElement(By.id(locator));
		} else if(driver.findElements(By.xpath(locator)).size() == 1) {
			return driver.findElement(By.xpath(locator)); 
		}  else if(driver.findElements(By.cssSelector(locator)).size() == 1) {
			return driver.findElement(By.cssSelector(locator));
		} else if(driver.findElements(By.className(locator)).size() == 1) {
			return driver.findElement(By.className(locator));
		} else if(driver.findElements(By.name(locator)).size() == 1) {
			return driver.findElement(By.name(locator));
		} 
		else
			throw new NoSuchElementException("No Such Element"+locator);
	}

	  /* Will wait until a certain element exists inside a WebDriver, searches using By selectors
	     * @param driver    the current WebDriver instance
	     * @param selector  the By selector we are searching for
	     * @param timeout   the number of seconds until this request times out
	     * @param sleep     the number of milliseconds between each poll
	     * @return the WebElement we have found
	     */
	    public static WebElement waitUntilExists(WebDriver driver, By selector, long timeout, long sleep){
	        try {
	        	Thread.sleep(sleep);
	            return (new WebDriverWait(driver, timeout, sleep)).until(ExpectedConditions.visibilityOfElementLocated(selector));
	        }
	        catch(TimeoutException e){
	            return null;
	        }
	        catch(Exception e){
//	            log().error("Unexpected error occurred while waiting for element to exist.");
	            return null;
	        }
	    }


	    @AfterClass(alwaysRun = true)
	    public void tearDown() throws Exception {
	      driver.quit();
	      String verificationErrorString = verificationErrors.toString();
	      if (!"".equals(verificationErrorString)) {
	        fail(verificationErrorString);
	      }
	    }
}