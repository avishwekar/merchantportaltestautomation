package com.code.java.selenium;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;

import com.code.java.api.GenericHelper;
import com.code.java.api.StartWebDriver;

import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class EndReward extends StartWebDriver {

  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();



  @Test
  public void testEndReward() throws Exception {
	  try {

    driver.findElement(By.linkText("Campaigns")).click();
    driver.findElement(By.linkText("Rewards")).click();

	driver.findElement(By.xpath("(//div//button[text()='End Reward'])[1]")).click();Thread.sleep(10000);
	driver.findElement(By.xpath("//div//button[text()='OK']")).click();Thread.sleep(8000);
    Thread.sleep(7000);
    System.out.println("Test Passed"+this.getClass());

	//Take Screenshot
	GenericHelper.takeScreenShot(this.getClass().toString());
	} catch(Exception e) {
		
		//Take Screenshot
		GenericHelper.takeScreenShot(this.getClass().toString());
		e.printStackTrace();
	}
  }



  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
