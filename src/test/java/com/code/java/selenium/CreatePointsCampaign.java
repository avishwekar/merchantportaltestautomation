package com.code.java.selenium;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;

import com.code.java.api.GenericHelper;
import com.code.java.api.StartWebDriver;
import com.code.java.api.TextBoxHelper;

import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CreatePointsCampaign extends StartWebDriver{

  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();


  @Test
  public void testCreatePointsCampaign() throws Exception {

	  try {

    driver.findElement(By.xpath("(//div//a//span)[2]")).click();Thread.sleep(8000);
    driver.findElement(By.xpath("//div//a//i[@class='icon-user-2']")).click();Thread.sleep(8000);
    driver.findElement(By.xpath("//a[position()=3]")).click();Thread.sleep(8000);
    driver.findElement(By.linkText("Campaigns")).click();
    driver.findElement(By.linkText("Points")).click();Thread.sleep(8000);

	driver.findElement(By.xpath("//button[text()='Create Campaign']")).click();Thread.sleep(10000);
	driver.findElement(By.xpath("(//input[@type='submit'])[1]")).click();Thread.sleep(8000);
    driver.findElement(By.xpath("(//button[@type='button'])[9]")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[73]")).click();
	driver.findElement(By.xpath("(//input[@type='submit'])[3]")).click();Thread.sleep(8000);
	driver.findElement(By.xpath("(//input[@type='submit'])[5]")).click();Thread.sleep(8000);
	driver.findElement(By.xpath("(//input[@type='submit'])[7]")).click();Thread.sleep(8000);
    driver.findElement(By.xpath("//div//button[text()='Done']")).click();
    Thread.sleep(7000);
    System.out.println("Test Passed"+this.getClass());

	//Take Screenshot
	GenericHelper.takeScreenShot(this.getClass().toString());
	} catch(Exception e) {
		
		//Take Screenshot
		GenericHelper.takeScreenShot(this.getClass().toString());
		e.printStackTrace();
	}
  }



  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
