package com.code.java.selenium;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.testng.annotations.*;

import com.code.java.api.GenericHelper;
import com.code.java.api.LinkHelper;
import com.code.java.api.StartWebDriver;
import com.code.java.api.TextBoxHelper;

import static org.testng.Assert.*;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class CreateOffer extends StartWebDriver {

  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();


  @Test
  public void testCreateOffer() throws Exception {

	  try {


	LinkHelper.clickLink("Campaigns");
	LinkHelper.clickLink("Offers");


	driver.findElement(By.xpath("//button[text()='Create an Offer']")).click();Thread.sleep(10000);
	driver.findElement(By.xpath("//div[@class='col-sm-4 col-sm-offset-1']//button")).click();Thread.sleep(8000);
	driver.findElement(By.xpath("//select/option[@value='5% Off']")).click();Thread.sleep(2000);

	TextBoxHelper.typeInTextBox("//input[@type='text']", "realityy");
	driver.findElement(By.xpath("//div[2]/button")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[7]")).click();
    driver.findElement(By.xpath("(//button[@type='button'])[73]")).click();
    driver.findElement(By.xpath("//button[2]")).click();
    driver.findElement(By.xpath("//textarea")).clear();
    driver.findElement(By.xpath("//textarea")).sendKeys("twenty");
    driver.findElement(By.xpath("//button[2]")).click();
    driver.findElement(By.xpath("//button[2]")).click();
    driver.findElement(By.xpath("//div[2]/div/button")).click();
    Thread.sleep(7000);
    System.out.println("Test Passed"+this.getClass());

	//Take Screenshot
	GenericHelper.takeScreenShot(this.getClass().toString());
	} catch(Exception e) {
		
		//Take Screenshot
		GenericHelper.takeScreenShot(this.getClass().toString());
		e.printStackTrace();
	}
  }


  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
